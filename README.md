Add a bookmark to your favourite browser with the following code:
```
javascript:(function(){document.getElementById("wiki").src="/wiki/"%20+%20document.getElementById("endpagelink0").children[0].href.split("/").slice(-1)[0]%20+%20"?"%20+%20document.getElementById("wiki").src.split("?")[1];})();
```
Enter a game. Press the bookmark. Done 👍
